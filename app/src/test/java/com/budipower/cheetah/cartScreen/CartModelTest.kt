package com.budipower.cheetah.cartScreen

import com.budipower.cheetah.data.CartDataModel
import com.budipower.cheetah.data.DataSource
import com.budipower.cheetah.data.remote.Cart
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.`when` as whenever

class CartModelTest {

    @Mock
    private lateinit var dataSource: DataSource<Cart?>

    private lateinit var target: CartModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        target = CartModel(dataSource)
    }

    @Test
    fun `when data source returns error, CartDataModel Error returned`() {

        val dummy :(Result<Cart?>)-> Unit = {}
        whenever(dataSource.fetchData(any() ?: dummy)).thenAnswer {
            val argument = it.arguments[0] as ((Result<Cart?>)-> Unit)
            argument.invoke(Result.failure(IllegalStateException()))
        }

        target.registerDataListener {
            assertThat(it).isSameInstanceAs(CartDataModel.Error)
        }
        target.loadData()
    }

    @Test
    fun `when data source returns Cart , CartDataModel Data returned`() {
        val dummy :(Result<Cart?>)-> Unit = {}
        whenever(dataSource.fetchData(any() ?: dummy)).thenAnswer {
            val argument = it.arguments[0] as ((Result<Cart?>)-> Unit)
            argument.invoke(Result.success(Cart(cartTotal = 1, orderItemsInformation = emptyList())))
        }

        target.registerDataListener {
            assertThat(it).isEqualTo(CartDataModel.Data(1.0.div(100), emptyList()))
        }
        target.loadData()
    }

    @Test
    fun `when data source returns null , empty CartDataModel Data returned`() {

        val dummy :(Result<Cart?>)-> Unit = {}
        whenever(dataSource.fetchData(any() ?: dummy)).thenAnswer {
            val argument = it.arguments[0] as ((Result<Cart?>)-> Unit)
            argument.invoke(Result.success(null))
        }

        target.registerDataListener { assertThat(it).isSameInstanceAs(CartDataModel.EMPTY) }
        target.loadData()
    }

    //todo more tests on data mapping and manipulation in success scenarios
}