package com.budipower.cheetah.cartScreen

import com.budipower.cheetah.data.CartDataModel
import com.budipower.cheetah.data.CartItem
import com.budipower.cheetah.data.remote.UnitType
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class CartPresenterTest {

    @Mock
    private lateinit var cartView: CartContract.View

    @Mock
    private lateinit var cartModel: CartModel

    @Captor
    private lateinit var doubleCaptor: ArgumentCaptor<Double>

    @Captor
    private lateinit var cartItemListCaptor: ArgumentCaptor<List<CartItem>>

    private lateinit var target: CartPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        target = CartPresenter(cartModel, cartView)
    }

    @Test
    fun `when presenter is created, a view is assigned with it and data listener registered`() {
        verify(cartView).presenter = target
        verify(cartModel).registerDataListener(target::onNewData)
    }

    @Test
    fun `when presenter is loaded, view hides not items and shows loading, model is loading data`() {
        target.load()

        verify(cartView).hideNoItems()
        verify(cartView).showLoading()
        verify(cartModel).loadData()
    }

    @Test
    fun `onNewData with any argument, hide loading is called`() {
        target.onNewData(CartDataModel.EMPTY)

        verify(cartView).hideLoading()
    }

    @Test
    fun `onNewData with error model, hide cart and show no items`() {
        target.onNewData(CartDataModel.Error)

        verify(cartView).hideCartHeader()
        verify(cartView).showNoItems()
    }

    @Test
    fun `onNewData with data model, hide cart and show no items with correct data`() {
        val testItems = listOf(CartItem("name", 1, 1, UnitType.UNIT,"uri", false))
        target.onNewData(CartDataModel.Data(1.1, testItems))

        verify(cartView).showCartHeader(doubleCaptor.capture())
        verify(cartView).showItems(cartItemListCaptor.capture()?: testItems)

        assertThat(doubleCaptor.value).isEqualTo(1.1)
        assertThat(cartItemListCaptor.value).isEqualTo(testItems)
    }
}
