package com.budipower.cheetah

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

@Suppress("UNCHECKED_CAST")
fun <T: View> ViewGroup.simpleInflate(@LayoutRes id: Int): T {
    return LayoutInflater.from(this.context).inflate(id, this, false) as T
}