package com.budipower.cheetah.cartScreen

import com.budipower.cheetah.data.CartDataModel
import com.budipower.cheetah.data.CartItem
import com.budipower.cheetah.data.DataSource
import com.budipower.cheetah.data.remote.*

class CartModel(private val dataSource: DataSource<Cart?>) : CartContract.Model {

    private lateinit var dataListener: (CartDataModel) -> Unit

    override fun registerDataListener(listener: (CartDataModel) -> Unit) {
        this.dataListener = listener
    }

    override fun loadData() = dataSource.fetchData { result ->
        result
            .onSuccess {
                val cartModel = it?.let { cart ->
                    val cartTotal = cart.cartTotal?.toDouble()?.div(100) ?: 0.0
                    val cartItems = cart.orderItemsInformation?.map(::cartItemMapper).orEmpty()

                    CartDataModel.Data(cartTotal, cartItems)
                } ?: CartDataModel.EMPTY

                dataListener(cartModel)
            }
            .onFailure {
                dataListener(CartDataModel.Error)
            }

    }

    private fun cartItemMapper(item: OrderItemsInformationItem?): CartItem = with(item) {
        CartItem(name, priceByType, quantity, unitType, photoUri, substitutable)
    }

}