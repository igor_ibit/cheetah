package com.budipower.cheetah.cartScreen

import androidx.annotation.VisibleForTesting
import com.budipower.cheetah.data.CartDataModel

class CartPresenter(private val model: CartContract.Model, private val view: CartContract.View) :
    CartContract.Presenter {

    init {
        model.registerDataListener(::onNewData)
        view.presenter = this
    }

    @VisibleForTesting
    fun onNewData(dataModel: CartDataModel) = with(view) {
        hideLoading()
        when (dataModel) {
            CartDataModel.Error -> {
                hideCartHeader()
                showNoItems()
            }
            is CartDataModel.Data -> {
                showCartHeader(dataModel.cartTotal)
                showItems(dataModel.cartItems)
            }
        }
    }

    override fun load() {
        view.hideNoItems()
        view.showLoading()
        model.loadData()
    }

}