package com.budipower.cheetah.cartScreen

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.budipower.cheetah.BaseViewHolder
import com.budipower.cheetah.R
import com.budipower.cheetah.data.CartItem
import com.budipower.cheetah.simpleInflate
import com.github.loadingview.LoadingView
import com.squareup.picasso.Picasso
import java.text.NumberFormat
import java.util.*


class CartFragment : Fragment(), CartContract.View {

    override lateinit var presenter: CartContract.Presenter
    private lateinit var loadingView: LoadingView
    private lateinit var list: RecyclerView
    private lateinit var emptyView: View
    private lateinit var cartHeader: TextView
    private val cartItems = mutableListOf<CartItem>()

    private val currencyFormat = NumberFormat.getCurrencyInstance().apply {
        maximumFractionDigits = 0
        currency = Currency.getInstance("USD")
    }

    companion object {
        fun newInstance(): CartFragment = CartFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_cart, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(view) {
            cartHeader = findViewById(R.id.cart_total)
            loadingView = findViewById(R.id.loading_view)
            emptyView = findViewById(R.id.empty_cart)
            list = findViewById<RecyclerView>(R.id.cart_items).apply {
                adapter = CartItemAdapter(cartItems, currencyFormat)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.load()
    }

    override fun showLoading() {
        loadingView.start()
    }

    override fun hideLoading() {
        loadingView.stop()
    }

    override fun hideNoItems() {
        emptyView.isVisible = false
    }

    override fun hideCartHeader() {
        cartHeader.isVisible = false
    }

    override fun showNoItems() {
        emptyView.isVisible = true
        list.isVisible = false
    }

    override fun showCartHeader(cartTotal: Double) {
        cartHeader.apply {
            isVisible = true
            text = getString(
                com.budipower.cheetah.R.string.cart_total_format,
                currencyFormat.format(cartTotal)
            )
        }
    }

    override fun showItems(cartItems: List<CartItem>) {
        emptyView.isVisible = false
        this.cartItems.apply {
            clear()
            addAll(cartItems)
        }
        list.adapter?.notifyDataSetChanged()
    }
}

class CartItemViewHolder(itemView: View, private val currencyFormat: NumberFormat) :
    BaseViewHolder<CartItem>(itemView) {

    private val name = itemView.findViewById<TextView>(R.id.name)
    private val pricePerUnit = itemView.findViewById<TextView>(R.id.price)
    private val totalPrice = itemView.findViewById<TextView>(R.id.total_price)
    private val quantity = itemView.findViewById<TextView>(R.id.quantity)
    private val productImage = itemView.findViewById<ImageView>(R.id.product_image)
    private val interchangeable = itemView.findViewById<ImageView>(R.id.interchangeable)

    private val pricePerUnitBuilder: SpannableStringBuilder = SpannableStringBuilder()
    private val unitColorSpan =
        ForegroundColorSpan(itemView.resources.getColor(android.R.color.holo_green_light, null))
    private val nonSubstitutableDrawable =
        itemView.resources.getDrawable(R.drawable.non_substitutable, null)
    private val substitutableDrawable =
        itemView.resources.getDrawable(R.drawable.substitutable, null)


    override fun bind(data: CartItem) {
        name.text = data.name
        totalPrice.text = currencyFormat.format(data.perTypePrice * data.quantity)
        quantity.text = data.quantity.toString()

        val drawable = if (data.substitutable) {
            substitutableDrawable
        } else {
            nonSubstitutableDrawable
        }

        interchangeable.setImageDrawable(drawable)

        pricePerUnit.text = pricePerUnitBuilder.apply {
            clear()
            append('$')
            append(data.perTypePrice.toString())
            append('/')
            append(data.packageType.toString(), unitColorSpan, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        if (data.photoUri.isNotEmpty()) {
            Picasso.get().load(data.photoUri)
                .placeholder(R.drawable.placeholder_drawable)
                .into(productImage)
        }
    }
}


class CartItemAdapter(
    private val cartItems: List<CartItem>,
    private val currencyFormat: NumberFormat
) : RecyclerView.Adapter<BaseViewHolder<CartItem>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<CartItem> =
        CartItemViewHolder(parent.simpleInflate(R.layout.cart_item), currencyFormat)

    override fun getItemCount(): Int = cartItems.size

    override fun onBindViewHolder(holder: BaseViewHolder<CartItem>, position: Int) =
        holder.bind(cartItems[position])
}