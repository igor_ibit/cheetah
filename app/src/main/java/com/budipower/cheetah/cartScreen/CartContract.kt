package com.budipower.cheetah.cartScreen

import com.budipower.cheetah.BasePresenter
import com.budipower.cheetah.BaseView
import com.budipower.cheetah.data.CartDataModel
import com.budipower.cheetah.data.CartItem

interface CartContract {
    interface View: BaseView<Presenter> {
        fun showLoading()
        fun hideCartHeader()
        fun showNoItems()
        fun showCartHeader(cartTotal: Double)
        fun showItems(cartItems: List<CartItem>)
        fun hideLoading()
        fun hideNoItems()
    }

    interface Presenter: BasePresenter {

    }

    interface Model {
        fun registerDataListener(listener: (CartDataModel) -> Unit)
        fun loadData()
    }

}