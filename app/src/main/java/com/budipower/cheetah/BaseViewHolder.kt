package com.budipower.cheetah

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T>(itemVIew: View): RecyclerView.ViewHolder(itemVIew) {
    abstract fun bind(data: T)
}