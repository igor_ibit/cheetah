package com.budipower.cheetah

interface BaseView<T> {
    var presenter: T
}
