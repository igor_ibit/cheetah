package com.budipower.cheetah

interface BasePresenter {
    fun load()
}
