package com.budipower.cheetah

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.budipower.cheetah.cartScreen.CartContract
import com.budipower.cheetah.cartScreen.CartFragment
import com.budipower.cheetah.cartScreen.CartModel
import com.budipower.cheetah.cartScreen.CartPresenter
import com.budipower.cheetah.data.CartDataSource

class MainActivity : AppCompatActivity() {

    private lateinit var cartPresenter: CartContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val cartFragment = supportFragmentManager.findFragmentById(R.id.content)
                as CartFragment? ?: CartFragment.newInstance().also {
            supportFragmentManager.commit { replace(R.id.content, it) }
        }

        cartPresenter = CartPresenter(CartModel(CartDataSource()), cartFragment)
    }
}

