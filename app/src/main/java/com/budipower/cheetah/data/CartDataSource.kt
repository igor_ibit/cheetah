package com.budipower.cheetah.data

import com.budipower.cheetah.data.remote.Cart
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

class CartDataSource : DataSource<Cart?> {

    private val webService by lazy {
        Retrofit.Builder()
            .baseUrl("http://www.mocky.io/v2/")
            .addConverterFactory(
                MoshiConverterFactory.create(
                    Moshi.Builder().add(
                        KotlinJsonAdapterFactory()
                    ).build()))
            .build()
            .create(WebService::class.java)
    }

    override fun fetchData(callBack: (Result<Cart?>) -> Unit) {
        webService.get().enqueue(object : Callback<Cart?> {
            override fun onFailure(call: Call<Cart?>, t: Throwable) = callBack(Result.failure(t))

            override fun onResponse(call: Call<Cart?>, response: Response<Cart?>) =
                callBack(Result.success(response.body()))
        })

    }

    interface WebService {
        @GET("59c791ed1100005300c39b93")
        fun get(): Call<Cart?>
    }
}