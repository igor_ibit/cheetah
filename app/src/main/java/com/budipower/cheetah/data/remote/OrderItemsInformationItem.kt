package com.budipower.cheetah.data.remote

import com.squareup.moshi.Json

data class OrderItemsInformationItem(

    @Json(name = "product")
    val product: Product? = null,

    @Json(name = "quantity")
    val quantity: Int? = null,

    @Json(name = "product_id")
    val productId: Int? = null,

    @Json(name = "packaging_type")
    val packagingType: String? = null,

    @Json(name = "sub_total")
    val subTotal: Int? = null,

    @Json(name = "id")
    val id: Int? = null,

    @Json(name = "substitutable")
    val substitutable: Boolean? = null,

    @Json(name = "order_id")
    val orderId: Int? = null
)

val OrderItemsInformationItem?.unitType: UnitType
    get() = this?.packagingType?.let { UnitType.valueOf(it.toUpperCase()) } ?: UnitType.UNIT

val OrderItemsInformationItem?.priceByType: Int
    get() = when (unitType) {
        UnitType.UNIT -> this?.product?.unitPrice ?: -1
        UnitType.CASE -> this?.product?.casePrice ?: -1
        UnitType.WEIGHT -> this?.product?.weightPrice ?: -1
    }


val OrderItemsInformationItem?.photoUri: String
    get() = when (unitType) {
        UnitType.UNIT -> this?.product?.unitPhotoFilename.orEmpty()
        UnitType.CASE -> this?.product?.packPhotoFile.orEmpty()
        UnitType.WEIGHT -> this?.product?.weightPhotoFilename.orEmpty()
    }

val OrderItemsInformationItem?.name: String
    get() = this?.product?.name.orEmpty()


val OrderItemsInformationItem?.quantity: Int
    get() = this?.quantity ?: -1

val OrderItemsInformationItem?.substitutable: Boolean
    get() = this?.substitutable ?: false
