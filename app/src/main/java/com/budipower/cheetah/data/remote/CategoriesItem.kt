package com.budipower.cheetah.data.remote

import com.squareup.moshi.Json

data class CategoriesItem(

	@Json(name="hide")
	val hide: Boolean? = null,

	@Json(name="parent_id")
	val parentId: Any? = null,

	@Json(name="image_url")
	val imageUrl: Any? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="display_order")
	val displayOrder: Int? = null,

	@Json(name="description")
	val description: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="image_id")
	val imageId: String? = null
)