package com.budipower.cheetah.data.remote

import com.squareup.moshi.Json

data class Cart(

	@Json(name="note")
	val note: String? = null,

	@Json(name="delivery_address")
	val deliveryAddress: String? = null,

	@Json(name="cart_total")
	val cartTotal: Int? = null,

	@Json(name="delivery_time_end")
	val deliveryTimeEnd: Int? = null,

	@Json(name="restaurant_id")
	val restaurantId: Int? = null,

	@Json(name="promo_code_discount")
	val promoCodeDiscount: Int? = null,

	@Json(name="delivery_date_iso8601")
	val deliveryDateIso8601: String? = null,

	@Json(name="promo_code_validation")
	val promoCodeValidation: Boolean? = null,

	@Json(name="promo_code")
	val promoCode: Any? = null,

	@Json(name="under_subscription")
	val underSubscription: Boolean? = null,

	@Json(name="delivery_fee")
	val deliveryFee: Int? = null,

	@Json(name="total")
	val total: Int? = null,

	@Json(name="available_delivery_times")
	val availableDeliveryTimes: List<AvailableDeliveryTimesItem?>? = null,

	@Json(name="local_time_iso8601")
	val localTimeIso8601: String? = null,

	@Json(name="created_at_iso8601")
	val createdAtIso8601: String? = null,

	@Json(name="error_description")
	val errorDescription: String? = null,

	@Json(name="free_delivery_discount")
	val freeDeliveryDiscount: Int? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="same_day_charge")
	val sameDayCharge: Int? = null,

	@Json(name="payment_method")
	val paymentMethod: String? = null,

	@Json(name="possible_fill_in")
	val possibleFillIn: Boolean? = null,

	@Json(name="subscription_fee")
	val subscriptionFee: Int? = null,

	@Json(name="same_day_charge_amount")
	val sameDayChargeAmount: Int? = null,

	@Json(name="fill_in")
	val fillIn: Boolean? = null,

	@Json(name="promo_code_discount_cash")
	val promoCodeDiscountCash: Int? = null,

	@Json(name="order_items_information")
	val orderItemsInformation: List<OrderItemsInformationItem?>? = null,

	@Json(name="delivery_date")
	val deliveryDate: String? = null,

	@Json(name="credit_card_charge")
	val creditCardCharge: Int? = null,

	@Json(name="first_delivery_discount")
	val firstDeliveryDiscount: Int? = null,

	@Json(name="sub_total")
	val subTotal: Int? = null,

	@Json(name="error_code")
	val errorCode: Any? = null,

	@Json(name="delivery_time_start")
	val deliveryTimeStart: Int? = null,

	@Json(name="last_time_modified_int")
	val lastTimeModifiedInt: Long? = null,

	@Json(name="status")
	val status: String? = null
)