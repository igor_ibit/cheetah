package com.budipower.cheetah.data

import com.budipower.cheetah.data.remote.UnitType

sealed class CartDataModel {
    object Error: CartDataModel()
    data class Data(val cartTotal: Double, val cartItems: List<CartItem>): CartDataModel()

    companion object {
        val EMPTY = Data(0.0, emptyList())
    }
}



data class CartItem(
    val name: String,
    val perTypePrice: Int,
    val quantity: Int,
    val packageType: UnitType,
    val photoUri: String,
    val substitutable: Boolean
)