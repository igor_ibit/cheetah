package com.budipower.cheetah.data.remote

import com.squareup.moshi.Json

data class AvailableDeliveryTimesItem(

	@Json(name="date")
	val date: String? = null,

	@Json(name="reserved")
	val reserved: Boolean? = null,

	@Json(name="start")
	val start: Int? = null,

	@Json(name="show_last_spots_left")
	val showLastSpotsLeft: Boolean? = null,

	@Json(name="end")
	val end: Int? = null,

	@Json(name="spots_left")
	val spotsLeft: Int? = null
)