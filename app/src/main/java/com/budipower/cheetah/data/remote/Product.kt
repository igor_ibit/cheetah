package com.budipower.cheetah.data.remote

import com.squareup.moshi.Json

data class Product(

    @Json(name = "unit_barcode")
    val unitBarcode: String? = null,

    @Json(name = "pack_photo_file")
    val packPhotoFile: String? = null,

    @Json(name = "weight_photo_hq_url")
    val weightPhotoHqUrl: String? = null,

    @Json(name = "items_per_unit")
    val itemsPerUnit: Int? = null,

    @Json(name = "available")
    val available: Boolean? = null,

    @Json(name = "weight_discount_threshold")
    val weightDiscountThreshold: Any? = null,

    @Json(name = "pack_barcode")
    val packBarcode: Any? = null,

    @Json(name = "unit_photo_filename")
    val unitPhotoFilename: String? = null,

    @Json(name = "avg_unit_weight")
    val avgUnitWeight: Any? = null,

    @Json(name = "unit_photo_hq_url")
    val unitPhotoHqUrl: String? = null,

    @Json(name = "id")
    val id: Int? = null,

    @Json(name = "categories")
    val categories: List<CategoriesItem?>? = null,

    @Json(name = "sku")
    val sku: String? = null,

    @Json(name = "pack_photo_hq_url")
    val packPhotoHqUrl: String? = null,

    @Json(name = "avg_case_weight")
    val avgCaseWeight: Any? = null,

    @Json(name = "updated_at_iso8601")
    val updatedAtIso8601: String? = null,

    @Json(name = "weight_orderable")
    val weightOrderable: Boolean? = null,

    @Json(name = "unit_price")
    val unitPrice: Int? = null,

    @Json(name = "weight_discount_price")
    val weightDiscountPrice: Any? = null,

    @Json(name = "weight_photo_filename")
    val weightPhotoFilename: String? = null,

    @Json(name = "grouped_by_category_name")
    val groupedByCategoryName: String? = null,

    @Json(name = "case_price")
    val casePrice: Int? = null,

    @Json(name = "units_per_case")
    val unitsPerCase: Int? = null,

    @Json(name = "name")
    val name: String? = null,

    @Json(name = "case_orderable")
    val caseOrderable: Boolean? = null,

    @Json(name = "weight_price")
    val weightPrice: Int? = null
)