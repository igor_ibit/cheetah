package com.budipower.cheetah.data

interface DataSource<T> {
    fun fetchData(callBack: (Result<T>)-> Unit)
}