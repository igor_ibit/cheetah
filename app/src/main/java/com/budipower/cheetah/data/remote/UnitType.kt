package com.budipower.cheetah.data.remote

enum class UnitType {
    UNIT, CASE, WEIGHT
}